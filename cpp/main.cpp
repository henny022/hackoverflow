#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <numbers>
#include <cmath>

void wave(pybind11::array_t<uint8_t, pybind11::array::c_style> image,
          pybind11::array_t<float, pybind11::array::c_style> audio)
{
    auto image_handle = image.mutable_unchecked<2>();
    auto audio_handle = audio.mutable_unchecked<1>();

    // hardcoded to match the sphere diameter
    auto r = 215;
    auto a = 40;
    auto n = 200000;
    auto offset = 1000000;// hardcoded offset into the wav
    for (int i = 0; i < n; ++i)
    {
        auto r2 = r + a * audio_handle(offset + i);
        auto phi = i * 2 * std::numbers::pi / n;
        auto x = 256 + (int) r2 * std::cos(phi);
        auto y = 256 + (int) r2 * std::sin(phi);
        image_handle(x, y) = 2; //mark as grey
    }
}

int value_for_amplitude(float amplitude)
{
    if (amplitude >= 0.5)
    { return 1; }
    if (amplitude <= -0.5)
    { return -1; }
    return 0;
}

int next_pos(int pos, int dim, int change)
{
    pos += change;
    return pos & (dim - 1);
}

void paint(auto image_handle, int x, int y)
{
    image_handle(x, y) = 0xff;
}

void rule1(auto image_handle, int x, int x_dim, int y, int y_dim)
{
    paint(image_handle, next_pos(x, x_dim, -1), next_pos(y, y_dim, 1));
    paint(image_handle, next_pos(x, x_dim, 0), next_pos(y, y_dim, 1));
    paint(image_handle, next_pos(x, x_dim, 1), next_pos(y, y_dim, 1));

    paint(image_handle, next_pos(x, x_dim, -1), next_pos(y, y_dim, 0));
    paint(image_handle, next_pos(x, x_dim, 0), next_pos(y, y_dim, 0));
    paint(image_handle, next_pos(x, x_dim, 1), next_pos(y, y_dim, 0));

    paint(image_handle, next_pos(x, x_dim, -1), next_pos(y, y_dim, -1));
    paint(image_handle, next_pos(x, x_dim, 0), next_pos(y, y_dim, -1));
    paint(image_handle, next_pos(x, x_dim, 1), next_pos(y, y_dim, -1));
}

void lines(pybind11::array_t<uint8_t, pybind11::array::c_style> image,
           pybind11::array_t<float, pybind11::array::c_style> audio)
{
    auto image_handle = image.mutable_unchecked<2>();
    auto audio_handle = audio.mutable_unchecked<2>();

    const auto x_dim = image_handle.shape(0);
    const auto y_dim = image_handle.shape(1);
    auto x = x_dim / 2;
    auto y = y_dim / 2;
    for (size_t i = 0; i < audio_handle.shape(0); ++i)
    {
        x = next_pos(x, x_dim, value_for_amplitude(audio_handle(i, 0)));
        y = next_pos(y, y_dim, value_for_amplitude(audio_handle(i, 1)));
        paint(image_handle, x, y);
    }
}

PYBIND11_MODULE(hackoverflow, m)
{
    m.doc() = "HackOverflow Creative Coding python binding";

    m.def("wave", &wave, "Add a circle with fix radius to the given numpy.ndarray.\n"
    "The circle has a constant value of 2 meaning it will be later rendered as dark grey.\n"
    "For rendering the resulting image (and storing it in pbm format) refer to convert_pixel_density.\n"
    "Meaning this method will take a numpy array in the custom 2bpp format of the size 512x512px.");
    m.def("lines", &lines, "Draw a line depending on the amplitude of the input wav-file of each channel.\n"
    "The left channel determines if the line will go left, right or stay while\n"
    "the right channel determines if the line will go up, down or stay at the current pixel and draw it white.\n"
    "This process is repeated for every data entry of the wav-file.\n"
    "There is no need to convert this result because it does not render greyscale.\n"
    "Meaning this method will take a numpy array in 8-Bit greyscale of the size 1024x1024px.");
}
