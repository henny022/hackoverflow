# HackOverflow
Team Eta Cyan

This is our solution for the HackOverflow Creative Coding Challenge.

It consists of a Python script and a C++ based python module.

## How to install
- Clone this project
  ```shell script
  git clone --recurse-submodules https://gitlab.com/henny022/hackoverflow.git
  cd hackoverflow
  ```
- set up a python environment using virtualenv
- install external dependencies
- ```shell script
  pip install -r requirements.txt 
  ```
- install the C++ module
  ```shell script
  pip install ./cpp
  ```

## Ho to use
- generate and display an image from a .wav file
  ```shell script
  python gameOfSound.py <wavfile> --show
  ```
- generate and save an image
  ```shell script
  python gameOfSound.py <wavfile> --output <output_file>
  ```
- generate and save an image as ascii pbm
  ```shell script
  python gameOfSound.py <wavfile> --output <output_file> --P1
  ```
