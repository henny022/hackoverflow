import argparse
import sys
from os import path

import cv2
import hackoverflow
import numpy
import pyrender
import scipy.io.wavfile
import trimesh.creation
from PIL import Image


def generate_sphere():
    sphere = trimesh.creation.icosphere(subdivisions=4, radius=0.8)
    sphere.vertices += 1e-2 * numpy.random.randn(*sphere.vertices.shape)
    mesh = pyrender.Mesh.from_trimesh(sphere, smooth=False)
    return mesh


def setup_scene():
    scene = pyrender.Scene(ambient_light=[.1, .1, .1], bg_color=[0, 0, 0])
    camera = pyrender.PerspectiveCamera(yfov=numpy.pi / 3.0)
    light = pyrender.DirectionalLight(color=[1, 1, 1], intensity=1.5e3)
    mesh = generate_sphere()

    scene.add(mesh, pose=numpy.eye(4))
    scene.add(light, pose=numpy.eye(4))

    c = 2 ** -0.5
    scene.add(camera, pose=[[1, 0, 0, 0],
                            [0, c, -c, -2],
                            [0, c, c, 2],
                            [0, 0, 0, 1]])
    return scene


def render_scene(scene, size):
    r = pyrender.OffscreenRenderer(size[0], size[1])
    color, _ = r.render(scene)
    return color


def _to2bpp(color):
    return color >> 6


to2bpp = numpy.vectorize(_to2bpp, otypes=[numpy.uint8])

pixelmap = {
    0: numpy.array([
        [0x00, 0x00],
        [0x00, 0x00],
    ], dtype=numpy.uint8),
    1: numpy.array([
        [0x00, 0x00],
        [0x00, 0xff],
    ], dtype=numpy.uint8),
    2: numpy.array([
        [0xff, 0x00],
        [0x00, 0xff],
    ], dtype=numpy.uint8),
    3: numpy.array([
        [0xff, 0xff],
        [0xff, 0xff],
    ], dtype=numpy.uint8),
}


def convert_pixel_density(image, bitdepth):
    width, height = image.shape
    final = numpy.empty((width * bitdepth, height * bitdepth), dtype=numpy.uint8)
    for x in range(width):
        for y in range(height):
            final[x * bitdepth:(x + 1) * bitdepth, y * bitdepth:(y + 1) * bitdepth] = pixelmap[image[x, y]]
    return final


def generate_2bpp_sphere(dim):
    scene = setup_scene()
    rendered_image = render_scene(scene, dim)
    sphere = to2bpp(cv2.cvtColor(rendered_image, cv2.COLOR_BGR2GRAY))
    return sphere


def dump_p1(data: numpy.ndarray, filename):
    with open(filename, 'w') as p1file:
        p1file.write('P1\n1024 1024\n')
        for row in data:
            for pixel in row:
                if pixel == 0xff:
                    p1file.write('0 ')
                else:
                    p1file.write('1 ')
            p1file.write('\n')


def image(array: numpy.ndarray, display=True, mode='L', location: path = None, output_p1=False):
    result = Image.fromarray(array, mode=mode)
    if location:
        if output_p1:
            dump_p1(array, location)
        else:
            result.save(location)
    if display:
        result.show()


def play(x_dim: int, y_dim: int, wav: path, output=None, output_p1=False, display=True):
    _, data = scipy.io.wavfile.read(wav)
    sphere = generate_2bpp_sphere((x_dim // 2, y_dim // 2))
    hackoverflow.wave(sphere, data[:, 0])
    image_data = convert_pixel_density(sphere, 2)
    hackoverflow.lines(image_data, data)  # TODO rule 1
    # show resulting image
    image(image_data, display=display, location=output, output_p1=output_p1)


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('wavfile')
    parser.add_argument('--output')
    parser.add_argument('--P1', action='store_true')
    parser.add_argument('--show', action='store_true')
    return parser.parse_args(argv)


def main(argv):
    args = parse_args(argv)
    play(x_dim=1024, y_dim=1024, wav=args.wavfile, output=args.output, output_p1=args.P1, display=args.show)


if __name__ == "__main__":
    main(sys.argv[1:])
